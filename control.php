<?php
include_once('model.php');
session_start();
class control extends model
{
	public function __construct()
	{
		model::__construct();
		$path = $_SERVER['PATH_INFO'];
		switch ($path) {
			case '/index':
				if (isset($_SESSION['customer'])) {
					echo "<script>location.href='customer_home'</script>";
				} else if (isset($_SESSION['distributer'])) {
					echo "<script>location.href='distributer_home'</script>";
				} else {
					if (isset($_REQUEST['submit'])) {
						$role = $_REQUEST['role'];
						$customer_name = $_REQUEST['user_name'];
						$email = $_REQUEST['email'];
						$pass = $_REQUEST['password'];
						$password = base64_encode($pass);
						$country = $_REQUEST['country'];
						$state = $_REQUEST['state'];
						$city = $_REQUEST['city'];
						$gender = $_REQUEST['gender'];
						$hobby = $_REQUEST['hobby'];
						$hobbies = implode(",", $hobby);
						$image = $_FILES['picture']['name'];
						$new = pathinfo($image, PATHINFO_EXTENSION);
						$rand = rand(0, 1000);
						$picture = "A." . date('d-m-Y') . $rand . "." . $new;
						move_uploaded_file($_FILES['picture']['tmp_name'], "picture/" . $picture);
						$data = array("name" => $customer_name, "email" => $email, "password" => $password, "country" => $country, "state" => $state, "city" => $city, "gender" => $gender, "hobbies" => $hobbies, "picture" => $picture);
						if ($role == "customer") {
							$result = $this->insert('customer_tbl', $data);
							$where = "email='{$email}' and password='{$password}'";
							$result = $this->select_where('customer_tbl', $where);
							$output = $result->fetch_object();
							$user_id = $output->customer_id;
						} else {
							$result = $this->insert('distributer_tbl', $data);
							$where = "email='{$email}' and password='{$password}'";
							$result = $this->select_where('distributer_tbl', $where);
							$output = $result->fetch_object();
							$user_id = $output->distributer_id;
						}

						if ($result) {
							$data = array("user_id" => $user_id, "role" => $role);
							$result = $this->insert('role_tbl', $data);
							$_SESSION['login'] = "Registration successfully completed....";
							echo "<script>location.href='login'</script>";
						} else {
							die($this->conn->error);
						}
					}
					include_once('index.php');
				}
				break;

			case '/login':
				if (isset($_SESSION['customer'])) {
					echo "<script>location.href='customer_home'</script>";
				} else if (isset($_SESSION['distributer'])) {
					echo "<script>location.href='distributer_home'</script>";
				} else {
					if (isset($_REQUEST['submit'])) {
						$email = $_REQUEST['email'];
						$pass = $_REQUEST['password'];
						$password = base64_encode($pass);
						$where = "email='{$email}' and password='{$password}'";
						$query1 = $this->select_where('customer_tbl', $where);
						$query2 = $this->select_where('distributer_tbl', $where);
						if ($query1->num_rows == 1) {
							$output = $query1->fetch_object();
							$c_name = $output->name;
							$_SESSION['customer'] = $c_name;
							if (isset($_REQUEST['save_data'])) {
								setcookie('email', $email, time() + 1 * 60);
								setcookie('password', $pass, time() + 1 * 60);
							}
							$_SESSION['customer_home'] = "Login successfully completed....";
							echo "<script>location.href='customer_home'</script>";
						} else if ($query2->num_rows == 1) {
							$output = $query2->fetch_object();
							$c_name = $output->name;
							$_SESSION['distributer'] = $c_name;
							if (isset($_REQUEST['save_data'])) {
								setcookie('email', $email, time() + 1 * 60);
								setcookie('password', $pass, time() + 1 * 60);
							}
							$_SESSION['distributer_home'] = "Login successfully completed....";
							echo "<script>location.href='distributer_home'</script>";
						} else {
							$_SESSION['login'] = "Your data is invalid....";
						}
					}
					include_once('login.php');
				}
				break;

			case '/customer_home':
				if (isset($_SESSION['customer'])) {
					$query1 = $_SESSION['customer'];
					$where = "name='{$query1}'";
					$result = $this->select_where('customer_tbl', $where);
					$output = $result->fetch_object();
					$where = "1=1";
					$data = $this->select_where('product_tbl', $where);
					$fetch_all = $data->fetch_all(MYSQLI_ASSOC);
					include_once('customer_home.php');
				} else {
					$_SESSION['login'] = "First login after access..";
					echo "<script>location.href='login'</script>";
				}
				break;

			case '/distributer_home':
				if (isset($_SESSION['distributer'])) {
					$query1 = $_SESSION['distributer'];
					$where = "name='{$query1}'";
					$result = $this->select_where('distributer_tbl', $where);
					$output = $result->fetch_object();
					$where = '1=1';
					$data = $this->select_where('product_tbl', $where);
					while ($data_all = $data->fetch_object()) {
						$abc[] = $data_all;
					}
					include_once('distributer_home.php');
				} else {
					$_SESSION['login'] = "First login after access..";
					echo "<script>location.href='login'</script>";
				}
				break;

			case '/add_product':
				if (isset($_SESSION['distributer'])) {
					$query1 = $_SESSION['distributer'];
					$where = "name='{$query1}'";
					$result = $this->select_where('distributer_tbl', $where);
					$output = $result->fetch_object();
					include_once('add_product.php');
					if (isset($_REQUEST['submit'])) {
						$product_brand = $_REQUEST['product_brand'];
						$product_name = $_REQUEST['product_name'];
						$price = $_REQUEST['price'];
						$quantity = $_REQUEST['quantity'];
						$description = $_REQUEST['description'];
						$product_category = $_REQUEST['product_category'];
						$image_id = "A." . rand(0, 1000) . ".B." . rand(0, 100) . ".C." . rand(0, 100);
						$data = array("product_brand" => $product_brand, "product_name" => $product_name, "product_image_id" => $image_id, "category_id" =>  $product_category, "product_price" => $price, "product_quantity" => $quantity, "product_description" => $description);
						$result = $this->insert('product_tbl', $data);
						foreach ($_FILES['picture']['tmp_name'] as $key => $value) {
							$picture_tmp_name = $_FILES['picture']['tmp_name'][$key];
							$picture_name = $_FILES['picture']['name'][$key];
							$new = pathinfo($picture_name, PATHINFO_EXTENSION);
							$rand = rand(0, 1000);
							$picture = "A." . date('d-m-Y') . $rand . "." . $new;
							move_uploaded_file($_FILES['picture']['tmp_name'][$key], "picture/" . $picture);
							$data = array("product_image_id" => $image_id, "image_name" => $picture, "product_image_id" => $image_id,);
							$this->insert("product_image_tbl", $data);
						}
						if ($result) {
							$_SESSION['distributer_home'] = "Add product successfully completed....";
							echo "<script>location.href='distributer_home'</script>";
						}
					}
				} else {
					$_SESSION['login'] = "First login after access..";
					echo "<script>location.href='login'</script>";
				}
				break;


			case '/add_cart':
				if (isset($_SESSION['customer'])) {
					$query1 = $_SESSION['customer'];
					$where = "name='{$query1}'";
					$result = $this->select_where('customer_tbl', $where);
					$output = $result->fetch_object();
					$c_id = $output->customer_id;
					$customer_id = array("customer_id" => $c_id);
					$c_quan = array("customer_quantity" => 1);
					$where = "product_id='{$_GET["product_id"]}'";
					$data1 = $this->select_where('product_tbl', $where);
					$fetch = $data1->fetch_assoc();
					$where = "product_id='{$_GET["product_id"]}' and customer_id='{$c_id}'";
					$data2 = $this->select_where('cart_tbl', $where);
					$fetch2 = $data2->fetch_object();
					if ($data2->num_rows == 1) {
						$old_quantity = $fetch2->customer_quantity;
						$total_quantity = $old_quantity + 1;
						$data = array("customer_quantity" => $total_quantity);
						$result = $this->update('cart_tbl', $data, $where);
					} else {
						$data = array_merge($fetch, $c_quan, $customer_id);
						$result = $this->insert('cart_tbl', $data);
					}
					if ($result) {
						$_SESSION['customer_home'] = "Your product successfully saved..";
						echo "<script>location.href='customer_home'</script>";
					}
				} else {
					$_SESSION['login'] = "First login after access..";
					echo "<script>location.href='login'</script>";
				}
				break;


			case '/my_cart':
				if (isset($_SESSION['customer'])) {
					$query1 = $_SESSION['customer'];
					$where = "name='{$query1}'";
					$result = $this->select_where('customer_tbl', $where);
					$output = $result->fetch_object();
					$customer_id = $output->customer_id;
					$where = "customer_id='{$customer_id}'";
					$data = $this->select_where('cart_tbl', $where);
					while ($abc = $data->fetch_object()) {
						$fetch_all[] = $abc;
					}
					include_once('my_cart.php');
				} else {
					$_SESSION['login'] = "First login after access..";
					echo "<script>location.href='login'</script>";
				}
				break;


			case '/product_page':
				if (isset($_SESSION['customer'])) {
					$query1 = $_SESSION['customer'];
					$where = "name='{$query1}'";
					$result = $this->select_where('customer_tbl', $where);
					$output = $result->fetch_object();

					$where = "product_image_id='{$_GET["product_image_id"]}'";
					$data = $this->select_where('product_tbl', $where);
					$fetch_data = $data->fetch_object();
					include_once('product_page.php');
				} else {
					$_SESSION['login'] = "First login after access..";
					echo "<script>location.href='login'</script>";
				}
				break;



			case '/view':
				if (isset($_REQUEST['product_image_id'])) {
					$product_image_id = $_REQUEST['product_image_id'];
					$where = "product_image_id='{$product_image_id}'";
					$data = $this->select_where('product_image_tbl', $where);
					while ($data_all = $data->fetch_object()) {
						$abc[] = $data_all;
					}
				}
				include_once('view.php');
				break;


			case '/delete':
				if (isset($_REQUEST['product_id'])) {
					$product_id = $_REQUEST['product_id'];
					$where = "product_id={$product_id}";

					$data = $this->select_where('product_tbl', $where);
					$fetch = $data->fetch_object();
					$product_image_id = $fetch->product_image_id;
					$where2 = "product_image_id='{$product_image_id}'";
					$data = $this->select_where('product_image_tbl', $where2);
					$fetch_data = $data->fetch_all(MYSQLI_ASSOC);
					foreach ($fetch_data as $d) {
						unlink("picture/" . $d['image_name']);
					}
					$result = $this->delete('product_tbl', $where);
					$result2 = $this->delete('product_image_tbl', $where2);
					if ($result) {
						$_SESSION['distributer_home'] = "Delete product successfully completed....";
						echo "<script>location.href='distributer_home'</script>";
					} else {
						$_SESSION['distributer_home'] = "Not Delete product....";
						echo "<script>location.href='distributer_home'</script>";
					}
				}

				if (isset($_REQUEST['cart_id'])) {
					$cart_id = $_REQUEST['cart_id'];
					$where = "cart_id='{$cart_id}'";
					$z = $this->delete('cart_tbl', $where);
					if ($z) {
						$_SESSION['add_to_cart'] = 'remove product successfully';
						echo "<script>location.href='my_cart'</script>";
					} else {
						$_SESSION['add_to_cart'] = 'Not delete product';
						echo "<script>location.href='my_cart'</script>";
					}
				}


				if (isset($_REQUEST['image_name'])) {
					$p_id = $_REQUEST['image_name'];
					$where = "image_name='{$p_id}'";
					$z = $this->delete('product_image_tbl', $where);
					if ($z) {
						unlink("picture/" . $p_id);
						$_SESSION['distributer_home'] = 'remove product successfully';
						echo "<script>location.href='distributer_home'</script>";
					} else {
						$_SESSION['distributer_home'] = 'Not delete product';
						echo "<script>location.href='distributer_home'</script>";
					}
				}
				break;


			case '/update':
				if (isset($_SESSION['distributer'])) {
					$query1 = $_SESSION['distributer'];
					$where = "name='{$query1}'";
					$result = $this->select_where('distributer_tbl', $where);
					$output = $result->fetch_object();
					$product_id = $_REQUEST['product_id'];
					$where1 = "product_id={$product_id}";

					$data = $this->select_where('product_tbl', $where1);
					$fetch = $data->fetch_object();
					$file_image = $fetch->product_image_id;
					$condition = "product_image_id='{$file_image}'";
					$data = $this->select_where('product_image_tbl', $condition);
					$data_all = $data->fetch_object();

					include_once('edit.php');
					if (isset($_REQUEST['submit'])) {
						$product_name = $_REQUEST['product_name'];
						$price = $_REQUEST['price'];
						$quantity = $_REQUEST['quantity'];
						$old_id = $_REQUEST['old_image'];
						$description = $_REQUEST['description'];
						$product_image_id = $fetch->product_image_id;


						$where = "product_image_id='{$file_image}'";
						$data = $this->select_where('product_image_tbl', $where);
						while ($d_all = $data->fetch_assoc()) {
							$array_keys[] = $d_all['image_name'];
						}
						$where = "product_image_id='{$file_image}'";
						$data = $this->select_where('product_image_tbl', $where);
						$d_data = $data->fetch_all(MYSQLI_ASSOC);
						$old = $_REQUEST['old'];
						foreach ($_REQUEST['old'] as $key => $value) {
							$picture_name1 = $d_data[$value]['product_image_id'];
							$picture_name2 = $d_data[$value]['image_name'];
							$output = in_array($picture_name2, $array_keys);
							if ($output == 1) {
								$array_image[] = $picture_name2;
							}
						}

						$a = array_diff($array_keys, $array_image);
						foreach ($a as $d) {
							unlink("picture/" . $d);
							$where = "image_name='{$d}'";
							$this->delete('product_image_tbl', $where);
						}
						if ($_FILES['image']['name'][0] != '') {
							foreach ($_FILES['image']['tmp_name'] as $key => $value) {
								$picture_tmp_name = $_FILES['image']['tmp_name'][$key];
								$picture_name = $_FILES['image']['name'][$key];
								$new = pathinfo($picture_name, PATHINFO_EXTENSION);
								$rand = rand(0, 1000);
								$picture = "A." . date('d-m-Y') . $rand . "." . $new;
								move_uploaded_file($_FILES['image']['tmp_name'][$key], "picture/" . $picture);
								$data = array("product_image_id" => $old_id, "image_name" => $picture);
								$this->insert("product_image_tbl", $data);
							}
						}

						$where = "product_image_id='{$file_image}'";
						$data = array("product_name" => $product_name, "product_price" => $price, "product_quantity" => $quantity, "product_description" => $description);
						$result = $this->update('product_tbl', $data, $where);

						if ($result) {
							$_SESSION['distributer_home'] = "Update product successfully completed....";
							echo "<script>location.href='distributer_home'</script>";
						} else {
							$_SESSION['distributer_home'] = "Not add product ....";
							echo "<script>location.href='distributer_home'</script>";
						}
					}
				} else {
					$_SESSION['login'] = "First login after access..";
					echo "<script>location.href='login'</script>";
				}
				break;

			case '/buy_product':
				if (isset($_SESSION['customer'])) {
					$query1 = $_SESSION['customer'];
					$where = "name='{$query1}'";
					$result = $this->select_where('customer_tbl', $where);
					$output = $result->fetch_object();
					$customer_id = $output->customer_id;
					$where = "customer_id='{$customer_id}'";
					$data = $this->select_where('buy_tbl', $where);
					while ($abc = $data->fetch_object()) {
						$fetch_all[] = $abc;
					}
					include_once('buy_product.php');
				} else {
					$_SESSION['login'] = "First login after access..";
					echo "<script>location.href='login'</script>";
				}
				break;

			case '/buy':
				if (isset($_REQUEST['product_id'])) {
					$quantity = $_REQUEST['quantity'];
					$price = $_REQUEST['iprice'];
					$product_id = $_REQUEST['product_id'];
					$query1 = $_SESSION['customer'];
					$where = "name='{$query1}'";
					$result = $this->select_where('customer_tbl', $where);
					$output = $result->fetch_assoc();
					$customer_id = $output['customer_id'];


					$where = "product_id={$product_id} and customer_id={$customer_id}";
					$result1 = $this->select_where('cart_tbl', $where);

					$result2 = $this->select_where('buy_tbl', $where);
					$fetch = $result2->fetch_object();
					if ($result2->num_rows == 1) {
						date_default_timezone_set('asia/calcutta');
						$date = date('d-m-Y');
						$time = date('h-i-s A');
						$old_quantity = $fetch->customer_quantity;
						$new_quantity = $old_quantity + $quantity;
						$data = array("customer_quantity" => $new_quantity, "date" => $date, "time" => $time);
						$result = $this->update('buy_tbl', $data, $where);
					} else {
						$result1 = $this->select_where('cart_tbl', $where);
						$output1 = $result1->fetch_array(MYSQLI_ASSOC);
						date_default_timezone_set('asia/calcutta');
						$time = date('h:i:s A');
						$date = date('d-m-Y');
						$output2 = array("customer_quantity" => $quantity, "customer_id" => $customer_id);
						$output3 = array("date" => $date, "time" => $time);
						$data = array_merge($output1, $output2, $output3);
						$result = $this->insert('buy_tbl', $data);
					}
					$wherea = "product_id={$product_id}";
					$output2 = $this->select_where('product_tbl', $wherea);
					$fetch2 = $output2->fetch_object();
					$product_quantity = $fetch2->product_quantity;
					$total_quantity = $product_quantity - $quantity;
					$data = array("product_quantity" => $total_quantity);
					$result = $this->update('product_tbl', $data, $wherea);

					if ($result) {
						$this->delete("cart_tbl", $where);
						$_SESSION['customer_home'] = 'Buy product successfully';
						echo "<script>location.href='customer_home'</script>";
					} else {
						$_SESSION['customer_home'] = 'Not buy product';
						echo "<script>location.href='customer_home'</script>";
					}
				}
				break;



			case '/logout':
				unset($_SESSION['customer']);
				unset($_SESSION['distributer']);
				$_SESSION['login'] = "Logout successfully completed...";
				echo "<script>location.href='login'</script>";
				break;

			default:
				include_once('page_not_found.php');
				break;
		}
	}
}
$object = new control();