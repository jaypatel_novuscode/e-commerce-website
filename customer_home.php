<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">

    <title>home</title>
</head>

<body>
    <?php
    if (isset($_SESSION['customer_home'])) {
    ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert" id="input">
        <strong>Hello!&nbsp;</strong><?php echo $_SESSION['customer_home']; ?>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
        </button>
    </div>

    <?php
        unset($_SESSION['customer_home']);
    }
    ?>
    <div class="container-fluid">
        <?php
        include('header.php');
        ?>
        <div class="row my-3">

            <div class="col-5">
                <form method="post" action="">
                    <div class="input-group mb-3 my-2	">
                        <select id="category_id" class="btn btn-outline-secondary dropdown-toggle">
                            <option class="dropdown-item" value=""><b>All</b></option>
                        </select>
                        <input type="text" class="form-control bg-light text-dark" name="search" id="search"
                            aria-label="Text input with dropdown button" placeholder="search..." style="width:300px;"
                            autocomplete="off">
                    </div>
                </form>
            </div>

        </div>
        <div class="row">
            <div class="col-2">

                <section id="sidebar">

                    <div class="border-bottom pb-2 ml-2">
                        <h4 id="burgundy">Filters</h4>
                    </div>

                    <div class="py-2 border-bottom ml-3">
                        <h6 class="font-weight-bold"><b>Price</b></h6>
                        <div id="slider"></div>
                        <p>
                            <input type="hidden" id="hidden_minimum_price" min=1 value="1" style="width: 100px;">
                            <input type="hidden" id="hidden_maximum_price" value="10000" style="width: 100px;" />
                            <input type="text" id="amount" readonly style="width: 100px;border:0; font-weight:bold;">
                        </p>

                        <div id="slider-range"></div><br>
                    </div>
                    <div class="py-2 border-bottom ml-3">

                    </div>
                </section>
            </div>
            <div class="col-10">
                <div class="row" id="product_list">

                    <?php
                    if ($data->num_rows > 0) {
                        foreach ($fetch_all as $d) {
                            $product_image_id = $d['product_image_id'];
                            $where1 = "product_image_id='{$product_image_id}'";
                            $data = $this->select_where('product_image_tbl', $where1);
                            $fetch = $data->fetch_assoc();
                    ?>

                    <div class="col-3  my-3">
                        <div class="card mx-auto" style="width:200px;">
                            <a href="product_page?product_image_id=<?php echo $product_image_id; ?>"><img
                                    src="picture/<?php echo $fetch['image_name']; ?>" class="card-img-top"
                                    style="height:200px;"></a>
                            <div class="card-body">
                                <div class="card-title text-center" style="font-weight: bold;">
                                    <?php echo $d['product_description']; ?>
                                </div>
                                <div class="card-title text-center">
                                    <b> Brand:</b> <?php echo $d['product_brand']; ?>
                                </div>
                                <div class="card-title text-center" style="font-weight: bold; color:red;">
                                    ₹ <?php echo $d['product_price']; ?>.00
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php
                        }
                    } else {
                        ?>
                    <div class="row">
                        <div class="col bg-light">
                            <h2 class="text-center">No Data Found</h2>
                        </div>
                    </div>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>

    <?php
    if (isset($_SESSION['customer_home'])) {
    ?>

    <?php
    }
    unset($_SESSION['customer_home']);
    ?>
    <?php
    include('footer.php')
    ?>
</body>

</html>