<?php

class model
{
	private $host_name='localhost';
	private $user_name='root';
	private $password='';
	private $db_name='task';
	private $conn='';

	public function __construct()
	{
		$this->conn=new mysqli($this->host_name,$this->user_name,$this->password,$this->db_name);
		if($this->conn->connect_error)
		{
			die("connection failed : " . $this->conn->connect_error);
			return false;
		}
		else{
			return true;
		}
	}



	public function select($tbl)
	{
		if($this->tbl_exists($tbl))
		{
			$query="select * from $tbl";
			$result=$this->conn->query($query);
			if($result)
			{	
				while($fetch=$result->fetch_object())
				{
					$arr[]=$fetch;
				}
				return $arr;
			}
			else
			{
				die("query faile :  ".$this->conn->error);
				return false;
			}

		}
	}

	public function insert($tbl,$data)
	{
		if($this->tbl_exists($tbl))
		{	
			$col=implode(",",array_keys($data));
			$val=implode("','",array_values($data));
			$query="insert into $tbl($col) values('$val')";
			$result=$this->conn->query($query);
			if($result)
			{	
				return $result;
			}
			else
			{
				die("query faile :  ".$this->conn->error);
				return false;
			}

		}
	}

	public function select_where($tbl,$where=null)
	{
		if($this->tbl_exists($tbl))
		{	
			$query="select * from $tbl where $where";
			$result=$this->conn->query($query);
			if($result)
			{	
				return $result;
			}
			else
			{	
				echo $query;
				die("query faile :  ".$this->conn->error);
				return false;
			}

		}
	}


	public function delete($tbl,$where=null)
	{
		if($this->tbl_exists($tbl))
		{	
			$query="delete from $tbl where $where";
			$result=$this->conn->query($query);
			if($result)
			{	
				return $result;
			}
			else
			{
				die("query faile :  ".$this->conn->error);
				return false;
			}

		}
	}


	public function update($tbl,$data,$where=null)
	{
		if($this->tbl_exists($tbl))
		{	

			$query="update $tbl set ";
			$temp=array();
			foreach($data as $key => $value)
			{
				$temp[]="$key='$value'";
			}
			$query.=implode(",",$temp);
			if($where != null)
			{
				$query.=" where $where";
			}
			$result=$this->conn->query($query);
			if($result)
			{	
				return $result;
			}
			else
			{
				die("query faile :  ".$this->conn->error);
				return false;
			}

		}
	}

	private function tbl_exists($tbl)
	{
		$query="SHOW TABLES FROM $this->db_name LIKE '%{$tbl}%'";
		$result=$this->conn->query($query);
		if($result->num_rows == 1)
		{
			return true;
		}
		else
		{
			die("TABLE : ".$tbl." does not exists");
		}
	}

	public function __destruct()
	{
		if($this->conn)
		{
			if($this->conn->close())
			{
				return true;
			}
			else{
				return true;
			}
		}	
	}
}