<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet">
	<title>home</title>
</head>
<body class="bg-light">
<?php
if(isset($_SESSION['delete']))
{
?>
	<div class="alert alert-success alert-dismissible fade show" role="alert" id="input">
 	 <strong>Hello!&nbsp;</strong><?php echo $_SESSION['delete'];?>
 	 <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
  	</button>
	</div>

<?php
unset($_SESSION['delete']);
}
?>
<div class="container-fluid">
	<div class="row my-3 bg-dark">
		<div class="col m-auto">
			<h2 class="text-danger text-center">Product list</h2>
		</div>	
	</div>

	<div class="row">
			<?php
			foreach($abc as $d)
			{
			?>
			<div class="col-4 text-center my-3">
				<div class="card mx-auto" style="width:200px;">
					<img src="picture/<?php echo $d->image_name;?>" class="card-img-top" style="height:200px;width:200px;">
					<div class="card-body">
						<a href="delete?image_name=<?php  echo $d->image_name; ?>" class="btn btn-danger" style="width:100%;" onclick="return confirm('are you sure')">Delete</a>
					</div>		
				</div>
			</div>
			<?php
			}
			?>
	</div>
	<div class="form-group  my-2">
		<a href="distributer_home" class="btn btn-dark" style="width: 100%;font-size: 17px;padding:5px 0;background-color: gray;">Back to home page</a>
	</div>
</div>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		setTimeout(function(){
			var a=document.querySelector(".alert");
			a.remove();
		},4000);
	});
</script>
</body>
</html>