<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Add product</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet">
</head>

<body class="bg-light">
    <div class="container-fluid">
        <?php
        include('dis_header.php');
        ?>
    </div>
    <div class="container">
        <form method="post" action="" autocomplete="off" enctype="multipart/form-data" id="reg">
            <div class="row">
                <div class="col-8 m-auto bg-light my-2" style="font-size:16px;font-family:arial;">

                    <div class="form-group  my-2">
                        <label for="picture">Product Category :</label>
                        <select name="product_category" id="product_category" class="form-control  my-2 py-2">
                            <option value="">Select the category</option>
                        </select>

                    </div>
                    <div class="form-group  my-2">
                        <label for="picture">Product Picture :</label>
                        <input type="file" name="picture[]" id="picture" class="form-control  my-2 py-2" multiple>
                    </div>

                    <div class="form-group  my-2">
                        <label for="product_brand">Product brand :</label>
                        <input type="text" name="product_brand" id="product_brand"
                            placeholder="please enter the product brand" class="form-control  my-2 py-2" char="aa">
                    </div>

                    <div class="form-group  my-2">
                        <label for="product_name">Product name :</label>
                        <input type="text" name="product_name" id="product_name"
                            placeholder="please enter the product name" class="form-control  my-2 py-2" char="aa">
                    </div>
                    <div class="form-group  my-2">
                        <label for="price">Price :</label>
                        <input type="text" name="price" id="price" placeholder="please enter the price"
                            class="form-control  my-2 py-2" autocomplete="off" numer="abc">
                    </div>
                    <div class="form-group  my-2">
                        <label for="quantity">Numer of product available :</label>
                        <input type="number" name="quantity" id="quantity" placeholder="please enter the quantity"
                            class="form-control  my-2 py-2" min=1>
                    </div>

                    <div class="form-group  my-2">
                        <label for="description">Description :</label>
                        <input type="text" name="description" id="description" style="height:60px;width:100%;">
                    </div>

                    <div class="form-group  my-2">
                        <input type="submit" value="submit" name="submit" class="btn"
                            style="width:100%;font-size: 17px;padding:5px 0;background-color:tomato;">
                    </div>
                </div>
            </div>
        </form>
    </div>


    <?php
    include('add_js.php');
    ?>
    <?php
    include('dis_footer.php');
    ?>
</body>

</html>