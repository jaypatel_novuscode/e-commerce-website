<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
    .update {
        background-color: tomato;
        color: black;
        padding: 7px 35px;
        cursor: pointer;
        font-weight: bold;
        font-size: 15px;
        text-decoration: none;
        border-radius: 20px;
    }
    </style>
    <title>home</title>
</head>

<body>
    <?php
    if (isset($_SESSION['add_to_cart'])) {
    ?>
    <div class="alert alert-success alert-dismissible" role="alert" id="liveAlert">
        <strong>Hello! </strong><?php echo $_SESSION['add_to_cart']; ?>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    <?php
        unset($_SESSION['add_to_cart']);
    }
    ?>
    <div class="container-fluid">
        <?php
        include('header.php');
        ?>
        <div class="row my-3">
            <div class="col text-center bg-light">
                <h3 style="font-weight:bold;">Cart list</h3>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <table class="table table-striped table-bordered table-hover">
                    <thead class="table-light">
                        <tr align="center" valign="middle">
                            <td>
                                Product image
                            </td>
                            <td>
                                Product name
                            </td>
                            <td style="width:400px;">
                                Product Information
                            </td>
                            <td>
                                Price
                            </td>
                            <td style="width:100px;">
                                Available Quantity
                            </td>
                            <td>
                                Quantity
                            </td>
                            <td style="width:200px;">
                                Sub total
                            </td>
                            <td style="width:300px;">
                                Action
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if ($data->num_rows > 0) {
                            foreach ($fetch_all as $d) {
                                $id = $d->product_image_id;
                                $where = "product_image_id='{$id}'";
                                $abc = $this->select_where('product_image_tbl', $where);
                                $xyz = $abc->fetch_assoc();
                        ?>
                        <form method="post" enctype="multipart/form-data"
                            action="buy?product_id=<?php echo $d->product_id; ?>">
                            <tr align="center" valign="middle" style="font-weight:bold;">
                                <td>
                                    <img src="picture/<?php echo $xyz['image_name']; ?>"
                                        style="width:90px;height:90px;">
                                </td>
                                <td>
                                    <?php echo $d->product_name; ?>
                                </td>
                                <td>
                                    <?php echo $d->product_description; ?>
                                </td>
                                <td>
                                    <?php echo "RS." . $d->product_price; ?>
                                    <input type="hidden" name="iprice" value="<?php echo $d->product_price; ?>"
                                        class="iprice">
                                </td>
                                <td>
                                    <?php echo $d->product_quantity; ?>
                                </td>
                                <td>
                                    <input type="number" name="quantity" value="<?php echo $d->customer_quantity; ?>"
                                        onchange="sum()" class="iquantity text-center" min=1
                                        max="<?php echo $d->product_quantity; ?>">
                                </td>
                                <td class="itotal">

                                </td>
                                <td>
                                    <input type="submit" name="submit" value="BUY" class="update">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <a href="delete?cart_id=<?php echo $d->cart_id; ?>"
                                        onclick="return confirm('are you sure ?')"><img src="picture/delete.jpg"
                                            style="width:60px;height:70px;">
                                    </a>
                                </td>
                            </tr>
                        </form>
                        <?php
                            }
                            ?>
                        <tr>
                            <td colspan="6">
                                <h3 style="font-weight:bold;float:right;font-size: 30px;">Total amount :</h3>
                            </td>
                            <td id="gtotal" colspan="2" style="font-weight:bold;font-size: 30px;">

                            </td>
                        </tr>
                        <?php
                        } else {
                        ?>
                        <tr align="center" valign="middle">
                            <td colspan="8">
                                <h2>Hello!Product list is empty.....</h2>
                            </td>
                        </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
    <?php
    if (isset($_SESSION['customer_home'])) {
    ?>

    <?php
    }
    unset($_SESSION['customer_home']);
    ?>
    <?php
    include('footer.php')
    ?>
</body>

</html>