<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <title>home</title>
</head>

<body>
    <?php
    if (isset($_SESSION['buy_product'])) {
    ?>
    <div class="alert alert-success alert-dismissible" role="alert" id="liveAlert">
        <strong>Hello! </strong><?php echo $_SESSION['buy_product']; ?>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    <?php
        unset($_SESSION['buy_product']);
    }
    ?>
    <div class="container-fluid">
        <div class="row my-3">
            <?php
            include('header.php');
            ?>

            <div class="row my-3">
                <div class="col text-center bg-light">
                    <h3 style="font-weight:bold;">Buy product list</h3>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <table class="table table-striped table-bordered table-hover">
                        <thead class="table-light">
                            <tr align="center" valign="middle">
                                <td>
                                    Product image
                                </td>
                                <td>
                                    Product name
                                </td>
                                <td style="width:400px;">
                                    Product Information
                                </td>
                                <td>
                                    Price
                                </td>
                                <td>
                                    Quantity
                                </td>

                                <td style="width:200px;">
                                    Date & Time
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($data->num_rows > 0) {
                                foreach ($fetch_all as $d) {
                                    $id = $d->product_image_id;
                                    $where = "product_image_id='{$id}'";
                                    $abc = $this->select_where('product_image_tbl', $where);
                                    $xyz = $abc->fetch_assoc();
                            ?>
                            <form method="post" enctype="multipart/form-data"
                                action="buy?cart_id=<?php echo $d->cart_id; ?>">
                                <tr align="center" valign="middle" style="font-weight:bold;">
                                    <td>
                                        <img src="picture/<?php echo $xyz['image_name']; ?>"
                                            style="width:90px;height:90px;">
                                    </td>
                                    <td>
                                        <?php echo $d->product_name; ?>
                                    </td>
                                    <td>
                                        <?php echo $d->product_description; ?>
                                    </td>
                                    <td>
                                        <?php echo "RS." . $d->product_price; ?>
                                    </td>
                                    <td>
                                        <?php echo $d->customer_quantity; ?>
                                    </td>

                                    <td>
                                        <?php echo "Date :" . $d->date . "<br>";
                                                echo "Time :" . $d->time; ?>
                                    </td>
                                </tr>

                                <?php
                                }
                            } else {
                                    ?>
                                <tr align="center" valign="middle">
                                    <td colspan="7">
                                        <h2>Hello!Product list is empty.....</h2>
                                    </td>
                                </tr>
                                <?php
                            }
                                ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <?php
        if (isset($_SESSION['customer_home'])) {
        ?>

        <?php
        }
        unset($_SESSION['customer_home']);
        ?>
        <?php
        include('footer.php')
        ?>

</body>

</html>