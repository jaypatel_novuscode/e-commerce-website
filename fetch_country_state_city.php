<?php
$conn = new mysqli('localhost', 'root', '', 'task');
if ($conn->connect_error) {
	die("connection failed : " . $conn->connect_error);
	return false;
}

if ($_POST['type'] == "product_category") {
	$query = "select * from product_category_tbl";
	$result = $conn->query($query);
	$output = "";
	if ($result) {
		while ($fetch_category = $result->fetch_object()) {
			$output .= "<option class='dropdown-item' value='{$fetch_category->category_id}'>{$fetch_category->category_name}</option>";
		}
		echo $output;
	} else {
		die("query faile :  " . $conn->error);
		return false;
	}
} else if ($_POST['type'] == "") {
	$query = "select * from country_tbl";
	$result = $conn->query($query);
	$output = "";
	if ($result) {
		while ($fetch_country = $result->fetch_object()) {
			$output .= "<option value='{$fetch_country->country_id}'>{$fetch_country->country_name}</option>";
		}
		echo $output;
	} else {
		die("query faile :  " . $conn->error);
		return false;
	}
} else if ($_POST['type'] == "state") {
	$query = "select * from state_tbl where country_id={$_POST['id']}";
	$result = $conn->query($query);
	$output = "<option value=''>select the state</option>";
	if ($result) {
		while ($fetch_state = $result->fetch_object()) {
			$output .= "<option value='{$fetch_state->state_id}'>{$fetch_state->state_name}</option>";
		}
		echo $output;
	} else {
		die("query faile :  " . $conn->error);
		return false;
	}
} else {
	$query = "select * from city_tbl where state_id={$_POST['id']}";
	$result = $conn->query($query);
	$output = "<option value=''>select the city</option>";
	if ($result) {
		while ($fetch_city = $result->fetch_object()) {
			$output .= "<option value='{$fetch_city->city_id}'>{$fetch_city->city_name}</option>";
		}
		echo $output;
	} else {
		die("query faile :  " . $conn->error);
		return false;
	}
}