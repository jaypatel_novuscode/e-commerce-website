<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Add product</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
    .my-error-class {
        color: red;
        font-weight: bold;
    }
    </style>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <!-- Image Uploader CSS -->
    <link rel="stylesheet" href="small/dist/image-uploader.min.css">
</head>

<body class="bg-light">
    <div class="container-fluid">
        <?php
        include('dis_header.php');
        ?>
    </div>
    <div class="container">
        <form method="post" action="" autocomplete="on" enctype="multipart/form-data" id="reg">
            <div class="row">
                <div class="col-8 m-auto bg-light my-2" style="font-size:16px;font-family:arial;">

                    <div class="form-group">
                        <div class="input-field">
                            <label class="active">photos</label>
                            <div name="jquery_pic" class="input-images" style="padding-top: .5rem;">
                                <input type="hidden" name="old_image" value="<?php echo $fetch->product_image_id; ?>">
                            </div>
                        </div>
                    </div>

                    <div class="form-group  my-2">
                        <label for="product_name">Product name :</label>
                        <input type="text" name="product_name" value="<?php echo $fetch->product_name; ?>"
                            id="product_name" placeholder="please enter the product name"
                            class="form-control  my-2 py-2" char="aa">
                    </div>
                    <div class="form-group  my-2">
                        <label for="price">Price :</label>
                        <input type="text" name="price" value="<?php echo $fetch->product_price; ?>" id="price"
                            placeholder="please enter the price" class="form-control  my-2 py-2" autocomplete="off"
                            numer="abc">
                    </div>
                    <div class="form-group  my-2">
                        <label for="quantity">Numer of product available :</label>
                        <input type="number" value="<?php echo $fetch->product_quantity; ?>" name="quantity"
                            id="quantity" placeholder="please enter the quantity" class="form-control  my-2 py-2" min=1>
                    </div>


                    <div class="form-group  my-2">
                        <label for="description">Description :</label>
                        <input type="text" name="description" value="<?php echo $fetch->product_description; ?>"
                            id="description" style="height:60px;width:100%;">

                    </div>

                    <div class="form-group  my-2">
                        <input type="submit" value="submit" name="submit" class="btn"
                            style="width:100%;font-size: 17px;padding:5px 0;background-color:tomato;">
                    </div>
                </div>
            </div>
        </form>
    </div>

    <?php
    $image_id = $data_all->product_image_id;
    $where = "product_image_id='{$image_id}'";
    $data = $this->select_where('product_image_tbl', $where);
    $arr = $data->fetch_all(MYSQLI_ASSOC);
    ?>
    <?php
    include('edit_js.php');
    ?>
    <?php
    include('dis_footer.php');
    ?>
</body>

</html>