<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet">

    <title>Document</title>
</head>

<body>

    <?php
    include_once('model.php');
    $object = new model();
    $search = $_REQUEST['search'];
    $minimum_price = $_REQUEST['minimum_price'];
    $maximum_price = $_REQUEST['maximum_price'];
    $category_id = $_REQUEST['category_id'];
    if (($_REQUEST['category_id'] != '') and ($_REQUEST['search'] != '')) {
        $where = "category_id='{$category_id}' AND product_name LIKE '%{$_REQUEST['search']}%' AND  product_price between {$minimum_price} AND {$maximum_price}";
    } else if (($_REQUEST['category_id'] != '') and ($_REQUEST['search'] == '')) {
        $where = "category_id='{$category_id}' AND  product_price between {$minimum_price} AND {$maximum_price}";
    } else if (($_REQUEST['category_id'] == '') and ($_REQUEST['search'] != '')) {
        $where = "product_name LIKE '%{$_REQUEST['search']}%' AND  product_price between {$minimum_price} AND {$maximum_price}";
    } else if (($_REQUEST['category_id'] == '') and ($_REQUEST['search'] == '')) {
        $where = "category_id  AND  product_price between {$minimum_price} AND {$maximum_price}";
    }


    $data = $object->select_where('product_tbl', $where);
    $fetch_all = $data->fetch_all(MYSQLI_ASSOC);
    ?>
    <div class="row">
        <?php
        if ($data->num_rows > 0) {
            foreach ($fetch_all as $d) {
                $product_image_id = $d['product_image_id'];
                $where1 = "product_image_id='{$product_image_id}'";
                $data = $object->select_where('product_image_tbl', $where1);
                $fetch = $data->fetch_assoc();
        ?>

        <div class="col-3 ">
            <div class="card mx-auto my-2" style="width:200px;">
                <a href="product_page?product_image_id=<?php echo $product_image_id; ?>"><img
                        src="picture/<?php echo $fetch['image_name']; ?>" class="card-img-top"
                        style="height:200px;"></a>
                <div class="card-body">
                    <div class="card-title text-center" style="font-weight: bold;">
                        <?php echo $d['product_name']; ?>
                    </div>
                    <div class="card-text">
                        <?php echo $d['product_description']; ?>
                    </div>
                    <br>
                    <div class="card-title text-center" style="font-weight: bold;">
                        <span style="color:red;font-weight:bold;"><?php echo "Price :" . $d['product_price']; ?></span>
                    </div>
                </div>

            </div>
        </div>
        <?php
            }
        } else {
            ?>
        <div class="row">
            <div class="col bg-light">
                <h2 class="text-center">No products available</h2>
            </div>
        </div>
        <?php
        }
        ?>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.min.js"></script>
</body>

</html>