<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <title>home</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <style>
    * {
        box-sizing: border-box;
    }

    body {
        margin: 0;
        font-family: Arial;
    }

    Four equal columns that floats next to each other / .column {
        float: left;
        width: 25%;
        padding: 10px;
    }

    Style the images inside the grid / .column img {
        opacity: 0.8;
        cursor: pointer;
    }

    .column img:hover {
        opacity: 1;
    }

    Clear floats after the columns / .row:after {
        content: "";
        display: table;
        clear: both;
    }

    The expanding image container / .container {
        position: relative;
        display: none;
    }

    Expanding image text / #imgtext {
        position: absolute;
        bottom: 15px;
        left: 15px;
        color: white;
        font-size: 20px;
    }

    Closable button inside the expanded image / .closebtn {
        position: absolute;
        top: 10px;
        right: 15px;
        color: white;
        font-size: 35px;
        cursor: pointer;
    }
    </style>
    <style>
    * {
        box-sizing: border-box;
    }

    body {
        margin: 0;
        font-family: Arial;
    }

    Four equal columns that floats next to each other / .column {
        float: left;
        width: 25%;
        padding: 10px;
    }

    Style the images inside the grid / .column img {
        opacity: 0.8;
        cursor: pointer;
    }

    .column img:hover {
        opacity: 1;
    }

    Clear floats after the columns / .row:after {
        content: "";
        display: table;
        clear: both;
    }

    The expanding image container / .container {
        position: relative;
        display: none;
    }

    Expanding image text / #imgtext {
        position: absolute;
        bottom: 15px;
        left: 15px;
        color: white;
        font-size: 20px;
    }

    Closable button inside the expanded image / .closebtn {
        position: absolute;
        top: 10px;
        right: 15px;
        color: white;
        font-size: 35px;
        cursor: pointer;
    }
    </style>

</head>

<body>
    <?php
    if (isset($_SESSION['customer_home'])) {
    ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert" id="input">
        <strong>Hello!&nbsp;</strong><?php echo $_SESSION['customer_home']; ?>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
        </button>
    </div>

    <?php
        unset($_SESSION['customer_home']);
    }
    ?>
    <div class="container-fluid">
        <?php
        include('header.php')
        ?>

        <div class="row my-3">
            <div class="col text-center bg-light">
                <h3 style="font-weight:bold;"><?php echo $fetch_data->product_name; ?></h3>
            </div>
        </div>
        <?php
        $product_image_id = $fetch_data->product_image_id;
        $where1 = "product_image_id='{$product_image_id}'";
        $data = $this->select_where('product_image_tbl', $where1);
        $result = $data->fetch_all(MYSQLI_ASSOC);
        ?>
        <div class="row">
            <div class="col-2">
                <?php
                foreach ($result as $d) {
                ?>
                <img src="picture/<?php echo $d['image_name']; ?>" style="width:80px;height:80px;"
                    onclick="myFunction(this);"><br><br>
                <?php
                }
                ?>
            </div>
            <div class="col-5 my-auto">
                <img src="picture/<?php echo $result[0]['image_name']; ?>" id="expandedImg" class="big_img"
                    style="width:300px;height:300px;">
                <div id="imgtext"></div>
            </div>
            <div class="col-5 my-auto">
                <table class="table table-light table-striped table-hover">
                    <tr align="center" valign="middle" class="table-dark">
                        <td colspan="2" style="font-weight:bold;color:white;">
                            Product Information:
                        </td>
                    </tr>
                    <tr align="center" valign="middle" style="color:black;font-weight: bold;">
                        <td>
                            Product brand :
                        </td>
                        <td>
                            <?php echo $fetch_data->product_brand; ?>
                        </td>
                    </tr>
                    <tr align="center" valign="middle" style="color:black;font-weight: bold;">
                        <td>
                            Product name :
                        </td>
                        <td>
                            <?php echo $fetch_data->product_name; ?>
                        </td>
                    </tr>
                    <tr align="center" valign="middle" style="color:black;font-weight: bold;">
                        <td>
                            Product price :
                        </td>
                        <td>
                            <?php echo $fetch_data->product_price; ?>
                        </td>
                    </tr>
                    <tr align="center" valign="middle" style="color:black;font-weight: bold;">
                        <td>
                            Quantity :
                        </td>
                        <td>
                            <?php echo $fetch_data->product_quantity; ?>
                        </td>
                    </tr>
                    <tr align="center" valign="middle" style="color:black;font-weight: bold;">
                        <td>
                            Information :
                        </td>
                        <td>
                            <?php echo $fetch_data->product_description; ?>
                        </td>
                    </tr>
                    <tr align="center" valign="middle">
                        <td colspan="2" style="font-weight:bold;color:white;">
                            <a href="add_cart?product_id=<?php echo $fetch_data->product_id; ?>" class="btn btn-warning"
                                style="color:white;font-weight:bold;font-size:20px;width:100%;">Add to cart</a>
                        </td>
                    </tr>

                </table>
            </div>
        </div>
    </div>
    <?php
    if (isset($_SESSION['customer_home'])) {
    ?>

    <?php
    }
    unset($_SESSION['customer_home']);
    ?>
    <?php
    include('footer.php')
    ?>

</body>

</html>