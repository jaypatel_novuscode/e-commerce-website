<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Registration page</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
    .my-error-class {
        color: red;
        font-weight: bold;
    }
    </style>
</head>

<body class="bg-light">
    <?php
	if (isset($_SESSION['login'])) {
	?>
    <div class="alert alert-success alert-dismissible fade show" role="alert" id="input">
        <strong>Hello!</strong><?php echo $_SESSION['login']; ?>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
        </button>
    </div>

    <?php
		unset($_SESSION['login']);
	}
	?>
    <div class="container">
        <div class="row">
            <div class="col-3 mx-auto my-4">
                <img src="picture/img_avatar.png" style="width:300px;height:300px;border-radius:30%;">
            </div>
        </div>
    </div>
    <div class="container">
        <form method="post" action="" autocomplete="on" enctype="multipart/form-data" id="reg">
            <div class="row">
                <div class="col-8 m-auto bg-light my-2" style="font-size:16px;font-family:arial;">
                    <div class="form-group  my-2">
                        <label for="email">Email :</label>
                        <input type="email" name="email" id="email" placeholder="please enter the email"
                            class="form-control  my-2 py-2" value="<?php if (isset($_COOKIE['email'])) {
																																					echo $_COOKIE['email'];
																																				} ?>">
                    </div>
                    <div class="form-group  my-2">
                        <label for="password">Password :</label>
                        <input type="password" pwcheck="" checkupper="" value="<?php if (isset($_COOKIE['password'])) {
																					echo $_COOKIE['password'];
																				} ?>" name="password" id="password" placeholder="please enter the password"
                            class="form-control  my-2 py-2">
                    </div>
                    <div class="form-group  my-2">
                        <input type="checkbox" onclick="pass1()" name="show_password" id="show_password">&nbsp;show
                        password
                    </div>
                    <div class="form-group  my-2">
                        <input type="checkbox" name="save_data" id="show_password">&nbsp;Remember me
                    </div>

                    <div class="form-group  my-2">
                        <input type="submit" value="Login" name="submit" class="btn btn-success"
                            style="width:100%;font-size: 17px;padding:5px 0">
                    </div>

                    <div class="form-group  my-2">
                        <a href="index" class="btn btn-primary"
                            style="width: 100%;font-size: 17px;padding:5px 0">Registration page</a>
                    </div>

                </div>
            </div>
        </form>
    </div>

    <script src="lib/jquery.js"></script>
    <script src="dist/jquery.validate.js"></script>
    <script>
    $(document).ready(function() {
        $("#reg").validate({
            errorClass: "my-error-class",
            rules: {
                email: {
                    required: true,
                    email: "email"
                },
                password: {
                    required: true,
                    minlength: 1,
                    maxlength: 12
                }
            },
            messages: {
                email: {
                    required: "! plz enter the email ",
                    email: "! plz enter the valid email address "
                },
                password: {
                    required: "! plz enter the password",
                    minlength: "! minimum 8 character is required",
                    maxlength: "! maximum 12 character is required"

                }

            }
        });
    });
    </script>
    <script type="text/javascript">
    function pass1() {
        var a = document.getElementById('password');
        if (a.type === "password") {
            a.type = "text";
        } else {
            a.type = "password";
        }
    }
    </script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        setTimeout(function() {
            var a = document.querySelector(".alert");
            a.remove();
        }, 4000);
    });
    </script>
</body>

</html>