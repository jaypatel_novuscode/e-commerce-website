<?php {
?>
<script src="https://kit.fontawesome.com/41ed9ed2cc.js" crossorigin="anonymous"></script>
<!-- bootstrp bundle file -->
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.min.js"></script>
<!-- jquery file -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<style type="text/css">
.my-error-class {
    color: red;
    font-weight: bold;
}
</style>

<script src="lib/jquery.js"></script>
<script src="dist/jquery.validate.js"></script>

<script>
$(document).ready(function() {
    $("#reg").validate({
        errorClass: "my-error-class",
        rules: {
            product_name: {
                required: true,
                char: true
            },
            price: {
                required: true,
                number: true
            },
            description: {
                required: true
            },
            quantity: {
                required: true,
            },
            'picture[]': {
                required: true
            },
            product_category: {
                required: true
            },
            product_brand: {
                required: true
            }

        },
        messages: {
            product_name: {
                required: "! plz enter the product name",
                char: "! plz enter only character.."
            },
            price: {
                required: "! plz enter the price",
                number: "! plz enter only numeric value"
            },
            description: {
                required: "! plz enter the description ",
            },
            product_category: {
                required: "plz select the category "
            },
            product_brand: {
                required: "plz select the category "
            },
            quantity: {
                required: "! plz enter the quantity ",
            },
            'picture[]': {
                required: "! plz select the product picture"
            }
        }
    });
    jQuery.validator.addMethod("char", function(value, element) {
        return this.optional(element) || /^[a-z A-Z.\s]+$/.test(value);
    }, 'Allow only A-Z & a-z . alphabet');
    $.validator.addMethod("number", function(value) {
        return /[0-9]/.test(value);
    });
});
</script>

<?php
}
?>