<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <title>home</title>
</head>

<body class="bg-light">
    <?php
    if (isset($_SESSION['distributer_home'])) {
    ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert" id="input">
        <strong>Hello!</strong><?php echo $_SESSION['distributer_home']; ?>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
        </button>
    </div>

    <?php

    }
    ?>
    <div class="container-fluid">
        <?php
        include('dis_header.php');
        ?>

    </div><br>
    <h2 style="text-align: center;"><b> Product List</b></h2>
    <div class="row">
        <div class="col">
            <table class="table table-striped table-bordered table-hover">
                <thead class="table-light">
                    <tr align="center" valign="middle">
                        <td>Product image</td>
                        <td>Product name</td>
                        <td>Product Information</td>
                        <td>Price</td>
                        <td>Quantity</td>
                        <td style="width:300px;">Action</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if ($data->num_rows > 0) {
                        foreach ($abc as $d) {
                            $id = $d->product_image_id;
                            $where = "product_image_id='{$id}'";
                            $abc = $this->select_where('product_image_tbl', $where);
                            $xyz = $abc->fetch_assoc();
                    ?>
                    <tr align="center" valign="middle">
                        <td>
                            <img src="picture/<?php echo $xyz['image_name']; ?>" style="width:90px;height:90px;"
                                alt="image not available">
                        </td>
                        <td>
                            <?php echo $d->product_name; ?>
                        </td>
                        <td>
                            <?php echo $d->product_description; ?>
                        </td>
                        <td>
                            <?php echo $d->product_price; ?>
                        </td>
                        <td>
                            <?php echo $d->product_quantity; ?>
                        </td>
                        <td>
                            <!-- <a href="view?product_image_id=<?php echo $d->product_image_id; ?>"><img
                                        src="picture/view.jpg" style="width:50px;height:50px;">
                                </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->
                            <a href="update?product_id=<?php echo $d->product_id; ?>"><i class="fas fa-edit">
                                </i></a>&nbsp;&nbsp;
                            <a href="delete?product_id=<?php echo $d->product_id; ?>"
                                onclick="return confirm('are you sure ?')"><i class="fas fa-trash"></i>
                            </a>
                        </td>
                    </tr>

                    <?php
                        }
                    } else {
                        ?>
                    <tr align="center" valign="middle">
                        <td colspan="6">
                            <h2>Hello!Product list is empty.....</h2>
                        </td>
                    </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    </div>

    <?php
    if (isset($_SESSION['distributer_home'])) {
    ?>

    <?php
    }
    unset($_SESSION['distributer_home']);
    ?>
    <?php
    include('dis_footer.php');
    ?>
</body>

</html>