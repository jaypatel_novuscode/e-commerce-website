<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
    * {
        box-sizing: border-box;
    }

    body {
        margin: 0;
        font-family: Arial, Helvetica, sans-serif;
    }

    .header {
        overflow: hidden;
        background-color: #EED4E1;
        padding: 20px 10px;
    }

    .header a {
        float: left;
        color: black;
        text-align: center;
        padding: 12px;
        text-decoration: none;
        font-size: 18px;
        line-height: 25px;
        border-radius: 4px;
    }

    .header a.logo {
        font-size: 25px;
        font-weight: bold;
    }

    .header a:hover {
        background-color: #B986AA;
        color: black;
    }

    .header a.active {
        background-color: dodgerblue;
        color: white;
    }

    .header-right {
        float: right;

    }

    @media screen and (max-width: 500px) {
        .header a {
            float: none;
            display: block;
            text-align: left;

        }

        .header-right {
            float: none;
        }
    }
    </style>
</head>

<body>
    <div class="header">

        <!-- <img src="picture/<?php echo $output->picture; ?>" style="border-radius: 50%;width:40px;height:40px;"> -->
        <a href="#" class="logo">Welcome <?php echo $_SESSION['distributer']; ?></a>
        <div class="header-right">
            <a href="distributer_home"><i class="fas fa-home"></i> Home</a>
            <a href="add_product"><i class="fas fa-plus-square"></i> Add Product</a>
            <a href="logout"> <i class="fas fa-sign-out-alt"></i> Logout</a>
        </div>
    </div>

</body>

</html>