<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<body>
    <style>
    * {
        box-sizing: border-box;
    }

    body {
        margin: 0;
        font-family: Arial, Helvetica, sans-serif;
    }

    .header {
        overflow: hidden;
        background-color: #EED4E1;
        padding: 20px 10px;
    }

    .header a {
        float: left;
        color: black;
        text-align: center;
        padding: 12px;
        text-decoration: none;
        font-size: 18px;
        line-height: 25px;
        border-radius: 4px;
    }

    .header a.logo {
        font-size: 25px;
        font-weight: bold;
    }

    .header a:hover {
        background-color: #B986AA;
        color: black;
    }

    .header a.active {
        background-color: dodgerblue;
        color: white;
    }

    .header-right {
        float: right;

    }

    @media screen and (max-width: 500px) {
        .header a {
            float: none;
            display: block;
            text-align: left;

        }

        .header-right {
            float: none;
        }
    }
    </style>
    </head>

    <body>
        <div class="header">

            <!-- <img src="picture/<?php echo $output->picture; ?>" style="border-radius: 50%;width:40px;height:40px;"> -->
            <a href="#" class="logo"><i> Welcome</i> &nbsp <?php echo $_SESSION['customer']; ?></a>
            <div class="header-right">
                <a href="customer_home"><i class="w3-jumbo w3-spin fa fa-home"></i> Home</a>
                <a href="buy_product"><i class="fas fa-clipboard-list"></i> Buy Product list</a>
                <a href="my_cart"><i class="fas fa-shopping-cart"></i> Cart</a>
                <a href="logout"><i class="fas fa-sign-out-alt"></i> Logout</a>
            </div>
        </div>

    </body>

</html>