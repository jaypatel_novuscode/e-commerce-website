<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Registration page</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
    .my-error-class {
        color: red;
        font-weight: bold;
    }
    </style>
</head>

<body>
    <div class="container">
        <form method="post" action="" autocomplete="on" enctype="multipart/form-data" id="reg">
            <div class="row">
                <div class="col-8 m-auto bg-light my-2" style="font-size:16px;font-family:arial;">
                    <div class="form-group">
                        <h2 class="text-center text-danger my-2">Registration form</h2>
                    </div>
                    <div class="form-group  my-2">
                        <label for="role">selct the role :</label>
                        <select name="role" id="role" class="form-control  my-2 py-2">
                            <option value="">please slect the role</option>
                            <option value="customer">customer</option>
                            <option value="distributer">Distributer</option>
                        </select>
                    </div>
                    <div class="form-group  my-2">
                        <label for="user_name">User name :</label>
                        <input type="text" name="user_name" uid="hello" id="user_name"
                            placeholder="please enter the user name" class="form-control  my-2 py-2">
                    </div>
                    <div class="form-group  my-2">
                        <label for="email">Email :</label>
                        <input type="email" name="email" id="email" placeholder="please enter the email"
                            class="form-control  my-2 py-2" autocomplete="off">
                    </div>
                    <div class="form-group  my-2">
                        <label for="password">Password :</label>
                        <input type="password" pwcheck="" checkupper="" name="password" id="password"
                            placeholder="please enter the password" class="form-control  my-2 py-2">
                    </div>
                    <div class="form-group  my-2">
                        <input type="checkbox" onclick="pass1()" name="show_password" id="show_password">&nbsp;show
                        password
                    </div>
                    <div class="form-group  my-2">
                        <label for="c_password">Confirm password :</label>
                        <input type="password" name="c_password" id="c_password"
                            placeholder="please enter the confirm password" class="form-control py-2">
                    </div>
                    <div class="form-group">
                        <input type="checkbox" name="show_password" onclick="pass2()" id="c_show_password">&nbsp;show
                        password
                    </div>
                    <div class="form-group  my-2">
                        <label for="country">Country :</label>
                        <select name="country" id="country" class="form-control  my-2 py-2">
                            <option value="">slect the country</option>
                        </select>
                    </div>
                    <div class="form-group  my-2">
                        <label for="state">State :</label>
                        <select name="state" id="state" class="form-control  my-2 py-2">
                        </select>
                    </div>
                    <div class="form-group  my-2">
                        <label for="city">city :</label>
                        <select name="city" id="city" class="form-control  my-2 py-2">
                        </select>
                    </div>
                    <div class="form-group  my-2">
                        <label for="picture">Picture :</label>
                        <input type="file" name="picture" id="picture" class="form-control  my-2 py-2">
                    </div>
                    <div class="form-group  my-2">
                        <label>Gender :</label>
                        <div class="form-check">
                            <label for="male">male</label><input type="radio" name="gender" id="male"
                                class="form-check-input" value="male">
                        </div>
                        <div class="form-check">
                            <label for="female">female</label>
                            <input type="radio" name="gender" id="female" class="form-check-input" value="female">
                        </div>
                        <div class="form-check">
                            <label for="other">other</label><input type="radio" name="gender" id="other"
                                class="form-check-input" value="other">
                        </div>

                    </div>

                    <div class="form-group  my-2">
                        <label>Hobbies :</label>
                        <div class="form-check">
                            <label for="chess">chess</label>
                            <input type="checkbox" name="hobby[]" id="chess" class="form-check-input" value="chess">
                        </div>
                        <div class="form-check">
                            <label for="criket">criket</label>
                            <input type="checkbox" name="hobby[]" id="criket" class="form-check-input" value="criket">
                        </div>
                        <div class="form-check">
                            <label for="football">football</label>
                            <input type="checkbox" name="hobby[]" id="football" class="form-check-input"
                                value="football">
                        </div>

                    </div>

                    <div class="form-group  my-2">
                        <input type="submit" value="submit" name="submit" class="btn btn-success"
                            style="width:100%;font-size: 17px;padding:5px 0">
                    </div>

                    <div class="form-group  my-2">
                        <a href="login" class="btn btn-primary" style="width: 100%;font-size: 17px;padding:5px 0">For
                            login click here</a>
                    </div>

                </div>
            </div>
        </form>
    </div>

    <script src="lib/jquery.js"></script>
    <script src="dist/jquery.validate.js"></script>
    <script>
    $(document).ready(function() {
        $("#reg").validate({
            errorClass: "my-error-class",
            rules: {
                role: {
                    required: true
                },
                user_name: {
                    required: true
                },
                email: {
                    required: true,
                    email: "email"
                },
                password: {
                    required: true,
                    minlength: 1,
                    maxlength: 12,
                    checkupper: true,
                    pwcheck: true
                },
                c_password: {
                    required: true,
                    minlength: 1,
                    maxlength: 12,
                    equalTo: "#password"
                },
                country: {
                    required: true
                },
                state: {
                    required: true
                },
                city: {
                    required: true
                },
                picture: {
                    required: true
                },
                gender: {
                    required: true
                },
                'hobby[]': {
                    required: true
                }
            },
            messages: {
                role: {
                    required: "! plz select the role otherwise you can not submit the form"
                },
                user_name: {
                    required: "! plz enter the user name"
                },
                email: {
                    required: "! plz enter the email ",
                    email: "! plz enter the valid email address "
                },
                password: {
                    required: "! plz enter the password",
                    minlength: "! minimum 8 character is required",
                    maxlength: "! maximum 12 character is required",
                    checkupper: "! minimum 1 uppercase letter is required",
                    pwcheck: "! Your password is weak ,plz pass minimum one '@' character"
                },
                c_password: {
                    required: "! plz enter the password",
                    minlength: "! minimum 8 character is required",
                    maxlength: "! maximum 12 character is required",
                    equalTo: "! Your confirm password value is diffrent , please pass same value"
                },
                picture: {
                    required: "! plz select the profile picture"
                },
                gender: {
                    required: "! plz select the gender"
                },
                'hobby[]': {
                    required: "! plz select at least one hobby"
                }
            }
        });
        jQuery.validator.addMethod("uid", function(value, element) {
            return this.optional(element) || /^[a-z A-Z.\s]+$/.test(value);
        }, 'Allow only A-Z & a-z . alphabet');
        $.validator.addMethod("checkupper", function(value) {
            return /[A-Z]/.test(value);
        });
        $.validator.addMethod("pwcheck", function(value) {
            return /[@]/.test(value);
        });
    });
    </script>
    <script type="text/javascript">
    function pass1() {
        var a = document.getElementById('password');
        if (a.type === "password") {
            a.type = "text";
        } else {
            a.type = "password";
        }
    }

    function pass2() {
        var a = document.getElementById('c_password');
        if (a.type === "password") {
            a.type = "text";
        } else {
            a.type = "password";
        }
    }
    </script>
    <script type="text/javascript">
    $(document).ready(function() {
        function load(type, id) {
            $.ajax({
                url: "fetch_country_state_city.php",
                type: "POST",
                data: {
                    type: type,
                    id: id
                },
                success: function(data) {
                    if (type == "state") {
                        $("#state").html(data);
                    } else if (type == "city") {
                        $("#city").html(data);
                    } else {
                        $("#country").append(data);
                    }
                }
            });
        }
        load();

        $("#country").on("change", function() {
            $('#state').html('');
            $('#city').html('');
            var c_id = $(this).val();
            if (c_id != "") {
                load("state", c_id);
            } else {
                $("#state").html('');
            }
        });

        $("#state").on("change", function() {
            var s_id = $(this).val();
            if (s_id != "") {
                load("city", s_id);
            } else {
                $("#city").html('');
            }
        });
    });
    </script>
</body>

</html>